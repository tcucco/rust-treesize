use std::process;
use treesize::args;
use treesize::map;
use treesize::out;

fn main() {
    let args = args::get_args();

    let root = args.value_of("root").unwrap().to_string();
    let number = if let Some(num_str) = args.value_of("number") {
        if let Ok(num) = num_str.parse::<usize>() {
            if num < 1 {
                eprintln!("number of lines must be a positive integer");
                process::exit(1);
            }
            num
        } else {
            eprintln!("number of lines must be a positive integer");
            process::exit(1);
        }
    } else {
        0
    };

    let map = if args.is_present("async") {
        map::map_treesize_async(&root)
    } else {
        map::map_treesize_sync(&root)
    };

    match map {
        Ok(map) => out::output_treesize(&map, number),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
}
