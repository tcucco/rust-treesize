use std::error::Error;
use std::fs;
use std::io;
use std::path::PathBuf;

pub fn walk_recursive<F>(dirname: &PathBuf, callback: &mut F)
where
    F: FnMut(fs::DirEntry),
{
    if let Ok(results) = fs::read_dir(dirname) {
        for result in results {
            if let Ok(entry) = result {
                if let Ok(file_type) = entry.file_type() {
                    if file_type.is_dir() {
                        walk_recursive(&entry.path(), callback);
                    }
                    callback(entry);
                }
            }
        }
    }
}

pub fn walk_recursive_pedantic<F>(dirname: &PathBuf, callback: &mut F) -> io::Result<()>
where
    F: FnMut(fs::DirEntry) -> io::Result<()>,
{
    match fs::read_dir(dirname) {
        Ok(results) => {
            for result in results {
                match result {
                    Ok(entry) => {
                        match entry.file_type() {
                            Ok(ft) => {
                                if ft.is_dir() {
                                    walk_recursive_pedantic(&entry.path(), callback)?;
                                }

                                callback(entry)?;
                            }
                            Err(err) => {
                                return Err(decorate_err("File type", &entry.path(), &err));
                            }
                        };
                    }
                    Err(err) => {
                        return Err(decorate_err("Read entry", dirname, &err));
                    }
                }
            }

            Ok(())
        }
        Err(err) => Err(decorate_err("Read dir", dirname, &err)),
    }
}

fn decorate_err(context: &str, dirname: &PathBuf, err: &io::Error) -> io::Error {
    let mut message = format!("{} error with {}: ", context, dirname.to_str().unwrap());
    message.push_str(err.description());
    io::Error::new(io::ErrorKind::Other, message)
}
