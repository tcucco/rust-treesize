use crate::walk;
use std::collections::HashMap;
use std::error;
use std::path::PathBuf;
use std::sync::mpsc;
use std::thread;

pub type MapKey = PathBuf;
pub type MapVal = u64;
pub type Map = HashMap<MapKey, MapVal>;

pub fn map_treesize_async(root: &String) -> Result<Map, Box<dyn error::Error>> {
    let root = PathBuf::from(root);
    let thread_root = root.clone();
    let mut map = Map::new();
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        walk::walk_recursive(&thread_root, &mut |de| {
            if let Ok(md) = de.metadata() {
                if md.is_file() {
                    let mut dir = de.path();
                    dir.pop();

                    let size = md.len();
                    ignore_result(tx.send((dir, size)));
                }
            }
        })
    });

    while let Ok((dir, size)) = rx.recv() {
        add_to_map(&mut map, &root, &dir, size);
    }

    Ok(map)
}

pub fn map_treesize_sync(root: &String) -> Result<Map, Box<dyn error::Error>> {
    let mut map = Map::new();
    let root = PathBuf::from(root);

    walk::walk_recursive(&root, &mut |de| {
        if let Ok(md) = de.metadata() {
            if md.is_file() {
                let mut dir = de.path();
                dir.pop();
                add_to_map(&mut map, &root, &dir, md.len());
            }
        }
    });

    Ok(map)
}

fn add_to_map(map: &mut Map, root: &PathBuf, dir: &PathBuf, size: u64) {
    let mut dir = dir.clone();

    let tot_size = map.entry(dir.clone()).or_insert(0);
    *tot_size += size;

    while dir.pop() {
        if !dir.starts_with(root) {
            break;
        }
        let tot_size = map.entry(dir.clone()).or_insert(0);
        *tot_size += size;
    }
}

pub fn ignore_result<T, U>(_: Result<T, U>) {}
