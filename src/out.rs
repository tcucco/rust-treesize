use crate::map::{Map, MapKey, MapVal};

pub fn output_treesize(map: &Map, limit: usize) {
    let mut sorted = map.iter().collect::<Vec<(&MapKey, &MapVal)>>();
    sorted.sort_by(|(ldir, lsiz), (rdir, rsiz)| {
        if lsiz == rsiz {
            ldir.cmp(rdir)
        } else {
            rsiz.cmp(lsiz)
        }
    });

    if limit > 0 {
        for item in sorted.iter().take(limit) {
            output_item(item);
        }
    } else {
        for item in sorted.iter() {
            output_item(item);
        }
    }
}

fn output_item(item: &(&MapKey, &MapVal)) {
    let (key, val) = item;
    println!("{}: {}", key.to_str().unwrap(), humanize_bytes(**val));
}

fn humanize_bytes(bytes: u64) -> String {
    let base = 1000;
    let base_half = base / 2;

    let (k, b) = (bytes / base, bytes % base);

    if k == 0 {
        return format!("{}B", b);
    }

    let (m, mut k) = (k / base, k % base);

    if m == 0 {
        if b >= base_half {
            k += 1;
        }
        return format!("{}K", k);
    }

    let (g, mut m) = (m / base, m % base);

    if g == 0 {
        if k >= base_half {
            m += 1;
        }
        return format!("{}M", m);
    }

    let (mut t, mut g) = (g / base, g % base);

    if t == 0 {
        if m >= base_half {
            g += 1;
        }
        return format!("{}G", g);
    }

    if g >= base_half {
        t += 1;
    }

    return format!("{}T", t);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bytes() {
        let bytes = 382;
        let output = humanize_bytes(bytes);
        assert_eq!("382B", output);
    }

    #[test]
    fn test_kilobytes() {
        let bytes = 3_820;
        let output = humanize_bytes(bytes);
        assert_eq!("4K", output);
    }

    #[test]
    fn test_megabytes() {
        let bytes = 18_450_888;
        let output = humanize_bytes(bytes);
        assert_eq!("18M", output);
    }

    #[test]
    fn test_gigabytes() {
        let bytes = 42_512_123_456;
        let output = humanize_bytes(bytes);
        assert_eq!("43G", output);
    }

    #[test]
    fn test_terabytes() {
        let bytes = 8_256_123_433_889;
        let output = humanize_bytes(bytes);
        assert_eq!("8T", output);
    }

    #[test]
    fn test_beyond_terabytes() {
        let bytes = 1_238_256_123_433_889;
        let output = humanize_bytes(bytes);
        assert_eq!("1238T", output);
    }
}
