use clap::{App, Arg, ArgMatches};

pub fn get_args<'a>() -> ArgMatches<'a> {
    App::new("Tree Size")
        .version("0.1")
        .author("Trey Cucco <fcucco@gmail.com>")
        .about("CLI program for finding large directories")
        .arg(Arg::with_name("root").required(true).takes_value(true))
        .arg(Arg::with_name("async").short("a").long("async"))
        .arg(
            Arg::with_name("number")
                .short("n")
                .long("number")
                .help("Number of lines to print")
                .takes_value(true),
        )
        .get_matches()
}
